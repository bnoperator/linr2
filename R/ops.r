lmOperator = function(x){
  aLm = lm(value.y ~ value.x, data = x)
  yFit = predict(aLm)
  yRes = residuals(aLm)
  yc = coef(aLm)[1] + yRes
  res = data.frame(colSeq = x$colSeq, yFit = yFit, yRes = yRes, yc = yc)
}
