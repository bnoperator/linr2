#' @import bnutil
#' @import shiny
#' @import dplyr
#' @export

#' @export
shinyServerRun = function(input, output, session, context) {

  output$body = renderUI({

    sidebarLayout(

      sidebarPanel(
                     helpText("Linear regression"),
                     selectInput("whatref", "Use as x:", c("X-Axis")),
                     actionButton("start", "Run"),
                     verbatimTextOutput("status")),
      mainPanel(
        plotOutput("pres")
      )
    )
  })



  getRunFolderReactive = context$getRunFolder()
  getStepFolderReactive = context$getFolder()
  getDataReactive = context$getData()

  observe({


    getData=getDataReactive$value
    if (is.null(getData)) return()

    getRunFolder = getRunFolderReactive$value
    if(is.null(getRunFolder)) return()

    getStepFolder = getStepFolderReactive$value
    if(is.null(getStepFolder)) return()

    bndata = getData()
    df = bndata$data

    if(bndata$hasXAxis){
      if(length(levels(factor(df$QuantitationType))) > 1){
        stop("No more than 1 qunatitation type allowed with X-Axis as reference.")
      }

    } else {
      stop("X-Axis required")
    }


    output$status = renderText({

      if(input$start >0){
        showNotification(ui = "Running LR...", type = "message", closeButton = FALSE, duration = NULL)
        if(!bndata$hasXAxis){
          dfx = df %>% filter(QuantitationType == input$whatref) %>% select(rowSeq, colSeq, value)
          dfy = df %>% filter(QuantitationType != input$whatref) %>% select(rowSeq, colSeq, value)
          dfin = left_join(dfx, dfy, by = c("rowSeq", "colSeq"))
        } else {
          dfin = df%>%mutate(value.y = value, value.x = df[[bndata$xAxisColumnName]]) %>% select(rowSeq, colSeq, value.x, value.y)
        }
        result = dfin %>%
          group_by(rowSeq) %>%
          do(lmOperator(.))


        meta.result = data.frame(labelDescription = c("rowSeq", "colSeq", "yFit", "yRes", "yc"),
                              groupingType = c("rowSeq", "colSeq", "QuantitationType", "QuantitationType", "QuantitationType"))
        aResult = AnnotatedData$new(data = result, metadata = meta.result)
        context$setResult(aResult)
        return("Done")
      }
    })
  })
}

#' @export
shinyServerShowResults = function(input, output, session, context){

}
