library(bnutil)
library(linreg2)
library(dplyr)



getdata = function() {
  dfile = system.file(
    "extdata",
    "aCubeDump.RData",
    package = "linreg2"
  )
  load(dfile)
  return(data)
}

setResult = function(annotatedResult){
  print(annotatedResult)
  result = annotatedResult$data
}

getProperties = function(){
  props = list(Interactive = "Yes")
}

bnMessageHandler = bnshiny::BNMessageHandler$new()
bnMessageHandler$getDataHandler = getdata
bnMessageHandler$setResultHandler = setResult
bnMessageHandler$getPropertiesAsMapHandler = getProperties


bnshiny::startBNTestShiny('linreg2', sessionType='run', bnMessageHandler=bnMessageHandler)
